const express = require("express");
// const mongoose = require("mongoose"); // Connector for MongoDB
// const bodyParser = require("body-parser"); // Let us use requests

var cors = require('express-cors');

var fs = require('fs');

require.extensions['.txt'] = function (module, filename) {
    module.exports = fs.readFileSync(filename, 'utf8');
};

const dictionaryTxt = require("./dictionary.txt");

const app = express();

// Middleware for BodyParser
// body-parser: extract the entire body portion of incoming request and exposes it on request.body
//app.use(bodyParser.json());

// MongoDB config og connection
// const db = require("./config/keys").mongoURI;

// mongoose
//   .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
//   .then(() => console.log("MongoDB Connected..."))
//   .catch(err => console.log(err));

// Uses the routes from routes/api/items.js
// app.use("/api/dictionary", dictionary);

app.use(cors({
  allowedOrigins: [
      'http://localhost:3000'
  ]
}))

app.get('/api/dictionary', function(req, res) {
  res.send(dictionaryTxt);
});

const port = 5000; // Sets port for server

app.listen(port, () => console.log(`Server started on port ${port}`));
