import PropTypes from 'prop-types';
import React from 'react';

const Phone = ({ topImage }) => (
    <div className="container">

        <div className="row">
            <div className="col-md-4">
                <div className="emulator">
                    <div><img className="img-thumbnail" src={topImage}></img></div>
                    <br></br>
                    <div className="screen">
                        <span className="prev-text"></span><span className="current-text"></span><span className="cursor">|</span>
                    </div>

                    <div className="controller">
                        <button className="prediction-cycle btn btn-primary"><span className="glyphicon glyphicon-refresh"></span> cycle</button>
                        <button className="delete pull-right btn btn-primary"><span className="glyphicon glyphicon-circle-arrow-left"></span> delete</button>
                    </div>

                    <div className="keypad">
                        <button className="key key-1" disabled="disabled" value="1">
                            1
              </button>
                        <button className="key key-2" value="2">
                            2 <small>abc</small>
                        </button>
                        <button className="key key-3" value="3">
                            3 <small>def</small>
                        </button>
                        <button className="key key-4" value="4">
                            4 <small>ghi</small>
                        </button>
                        <button className="key key-5" value="5">
                            5 <small>jkl</small>
                        </button>
                        <button className="key key-6" value="6">
                            6 <small>mno</small>
                        </button>
                        <button className="key key-7" value="7">
                            7 <small>pqrs</small>
                        </button>
                        <button className="key key-8" value="8">
                            8 <small>tuv</small>
                        </button>
                        <button className="key key-9" value="9">
                            9 <small>wxyz</small>
                        </button>
                        <button className="key key-star" disabled="disabled" value="symbol">
                            * #
              </button>
                        <button className="key key-0" disabled="disabled" value="0">
                            0
              </button>
                        <button className="key key-space" value="space">
                            <small>space</small>
                        </button>
                    </div>
                </div>
                <div className="loading" style={{ marginTop: "10em" }}>
                    <div className="progress progress-striped active">
                        <div className="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style={{ width: "0%" }}>
                        </div>
                    </div>
                    <p>Downloading dictionary file. Please wait...</p>
                </div>

            </div>

        </div>

    </div>
);

Phone.PropTypes = {
    topImage: PropTypes.string,
};

Phone.defaultProps = {
    topImage: ``,
};

export default Phone;