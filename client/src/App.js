import React, { Component } from "react";
import { Provider } from "react-redux";

import store from "./store";

import Phone from './components/Phone';

import logo from './images/logo_genius_green.jpg';

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/css/bootstrap-theme.min.css";
import "./App.css";
import "./t9/script.js";


class App extends Component {
  render() {
    return (
      // Provider: wraps the React application and makes the Redux state available to all container components in the application’s hierarchy
      <Provider store={store}>
        <div className="">
          <Phone
            topImage={logo}
          ></Phone>
        </div>
      </Provider>
    );
  }
}

export default App;
