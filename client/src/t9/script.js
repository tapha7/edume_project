import $ from 'jquery';
let _ = require('underscore');

//The t9 processing could be done via the backend or the frontend. I opted to do it on the frontend so as to save time.

var t9 = {};

t9.dictionary = '';
t9.dictionaryTree = {};
t9.words = [];
t9.keyMap = {
    2: 'abc',
    3: 'def',
    4: 'ghi',
    5: 'jkl',
    6: 'mno',
    7: 'pqrs',
    8: 'tuv',
    9: 'wxyz'
};

function Word(word, occurrences) {
    this.word = word;
    this.occurrences = occurrences;
}

Word.prototype.toString = function () {
    return this.word + ' (' + this.occurrences + ')';
};

t9.init = function (dictionary) {
    console.log('initializing dictionary');
    t9.dictionary = dictionary;
    t9.words = dictionary.split(/\s+/g);

    var tree = {};

    // https://github.com/jrolfs/javascript-trie-predict/blob/master/predict.js
    t9.words.forEach(function (word) {
        var letters = word.split('');
        var leaf = tree;

        for (var i = 0; i < letters.length; i++) {
            var letter = letters[i].toLowerCase();
            var existing = leaf[letter];
            var last = (i === letters.length - 1);

            // If child leaf doesn't exist, create it
            if (typeof (existing) === 'undefined') {
                // If we're at the end of the word, mark with number, don't create a leaf
                leaf = leaf[letter] = last ? 1 : {};

                // If final leaf exists already
            } else if (typeof (existing) === 'number') {
                // Increment end mark number, to account for duplicates
                if (last) {
                    leaf[letter]++;

                    // Otherwise, if we need to continue, create leaf object with '$' marker
                } else {
                    leaf = leaf[letter] = { $: existing };
                }

                // If we're at the end of the word and at a leaf object with an
                // end '$' marker, increment the marker to account for duplicates
            } else if (typeof (existing) === 'object' && last) {
                if (existing.hasOwnProperty('$')) {
                    leaf[letter].$++;
                } else {
                    leaf[letter] = existing;
                    leaf[letter].$ = 1;
                }

                // Just keep going
            } else {
                leaf = leaf[letter];
            }
        }

    });

    t9.dictionaryTree = tree;

};

t9.predict = function (numericInput) {
    var input = new String(numericInput);
    var results = t9.findWords(numericInput, t9.dictionaryTree, true);

    return results;
};


t9.findWords = function (sequence, tree, exact, words, currentWord, depth) {

    var current = tree;

    sequence = sequence.toString();
    words = words || [];
    currentWord = currentWord || '';
    depth = depth || 0;

    // Check each leaf on this level
    for (var leaf in current) {
        var word = currentWord;
        var value = current[leaf];
        var key;

        // If the leaf key is '$' handle things one level off since we
        // ignore the '$' marker when digging into the tree
        if (leaf === '$') {
            key = sequence.charAt(depth - 1);
            if (depth >= sequence.length) {
                words.push(word);
            }
        } else {
            key = sequence.charAt(depth);
            word += leaf;
            if (depth >= (sequence.length - 1) && typeof (value) === 'number' && key && (t9.keyMap[key].indexOf(leaf) > -1)) {
                words.push(word);
            }
        }

        // If the leaf's value maps to our key or we're still tracing
        // the prefix to the end of the tree (`exact` is falsy), then
        // "we must go deeper"...

        if ((key && t9.keyMap.hasOwnProperty(key) && t9.keyMap[key].indexOf(leaf) > -1) || (!key && !exact)) {
            t9.findWords(sequence, value, exact, words, word, depth + 1);
        }
    }

    // Yeah, not as cool when not returning the recursive function call
    // returns, but we gotta just rely on JS references since we may be
    // going more than one way down the tree and we don't want to be
    // breaking the leaf loop
    return words;
};

(function() {
    var root = this;
    var currentWord = '';
    var currentPredictions = [];
    var currentPredictionsIndex = 0;

    $(function(){
        $.ajax({
            url: 'http://localhost:5000/api/dictionary',
            success: function(data, textStatus, jqXHR ) {
                $('.loading p').text('Dictionary file downloaded!');
                _.delay(function(){
                    $('.loading').fadeOut();
                }, 1000);

                t9.init(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.loading p').text('Error loading dictionary file! Try reloading the page.');
            },
            xhr: function(){
                var xhr = new XMLHttpRequest();
                xhr.addEventListener("progress", function(event){
                    if (event.lengthComputable) {  
                     var percentComplete = 100 * event.loaded / event.total;
                     console.log('dictionary loading at', percentComplete, '%');
                     $('.loading .progress-bar')
                        .attr('aria-valuenow', percentComplete)
                        .css('width', percentComplete + '%');
                    }
                }, false); 

                return xhr;
            }
        });

        $('button.key').on('click', function(event){
            var value = $(this).val();
            if(value == 'space'){
                currentWord = '';
                currentPredictions = [];
                currentPredictionsIndex = 0;
                var previousText = $('.prev-text').text() + ' ' + $('.current-text').text() + ' ';
                $('.prev-text').text(previousText);
                $('.current-text').text('');
            }

            if(isNaN(parseInt(value)))
                return;

            currentWord += value;
            currentPredictions = t9.predict(currentWord);
            currentPredictionsIndex = 0;
            if(currentPredictions.length > 0){
                $('.current-text').text(currentPredictions[0]);
            }
            else {
                $('.current-text').text($('.current-text').text() + value);
            }
        });

        $('.controller .prediction-cycle').on('click', function(event){
            if(currentPredictions.length > 0){
                if (++currentPredictionsIndex >= currentPredictions.length)
                    currentPredictionsIndex = 0;

                $('.current-text').text(currentPredictions[currentPredictionsIndex]);
            }
        });

        $('.controller .delete').on('click', function(event){
            if(currentWord == ''){
                var previousText = $('.prev-text').text();
                previousText = previousText.slice(0, previousText.length-1);
                $('.prev-text').text(previousText);
                return;
            }

            currentWord = currentWord.slice(0, currentWord.length-1);
            currentPredictions = t9.predict(currentWord);
            currentPredictionsIndex = 0;
            if(currentPredictions.length > 0){
                $('.current-text').text(currentPredictions[0]);
            }
            else {
                var currentText = $('.current-text').text();
                currentText = currentText.slice(0, currentText.length-1);
                $('.current-text').text(currentText);
            }
        });

        $('body').on('keydown', function(event) {
            if (event.keyCode === 8) {
                event.preventDefault();
                console.log(currentWord);
            }

            switch (String.fromCharCode(event.keyCode)) {
                case "2":
                    $('button.key-2').focus().click();
                    break;
                case "3":
                    $('button.key-3').focus().click();
                    break;
                case "3":
                    $('button.key-3').focus().click();
                    break;
                case "4":
                    $('button.key-4').focus().click();
                    break;
                case "5":
                    $('button.key-5').focus().click();
                    break;
                case "6":
                    $('button.key-6').focus().click();
                    break;
                case "7":
                    $('button.key-7').focus().click();
                    break;
                case "8":
                    $('button.key-8').focus().click();
                    break;
                case "9":
                    $('button.key-9').focus().click();
                    break;
                case " ":
                    $('button.key-space').focus().click();
                    break;
                case String.fromCharCode(192):
                    $('button.prediction-cycle').focus().click();
                    break;
                case String.fromCharCode(8):
                    $('button.delete').focus().click();
                    break;
            }
        });

    });

}).call(this);

// Credit/Modified - https://github.com/arifwn/t9-emulator/blob/master/js/script.js
// Credit/Modified - https://github.com/arifwn/t9-emulator/blob/master/js/t9.js

// Credit/Modified, means I have leveraged some of the code from these files to save time and have also modified them 
// to suit my needs here.