## EduMe Project

![EduMe](https://imgur.com/52ip26C.png)

### How To Install

After running git clone, to clone the project, navigate to the `edume_project` directory and run `npm i`. This will install the main node/express side of the project. 

Once this has been installed, then navigate to the `client` directory within the `edume_project` directory and run `npm i` again to install the React side of the project.

#### Note

I have opted to place the t9 processing code within the frontend React app for time-savings, as this is simply for demonstration purposes.

This project leverages code from other sources, this is referenced where this is relevant within the code.

Commits are not visible as I had to create a new git file for this project after the fact.

The project structure is built from a boilerplate.

[https://github.com/JesperBry/MERN-boilerplate](https://github.com/JesperBry/MERN-boilerplate).

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) (this is where you'll see the main app.) or [http://localhost:5000](http://localhost:5000), this is the backend code, to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.